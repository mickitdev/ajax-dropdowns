$(function () {

    $("#SurveyContainer").find("select[name='CountryId']")
        .cascadeDropdown({
            from: "RegionId",
            url: "https://restcountries.eu/rest/v2/region/",
            parameters: function () {
                return $("#SurveyContainer").find("select[name='RegionId']").val();
            },
            value: "alpha3Code"
        });

    $("#SurveyContainer").find("select[name='LanguageId']")
        .cascadeDropdown({
            from: "CountryId",
            url: "https://restcountries.eu/rest/v2/alpha/",
            parameters: function () {
                return $("#SurveyContainer").find("select[name='CountryId']").val();
            },
            property: "languages",
            value: "iso639_1",
            name: "name"
        });

    $("#SurveyContainer").find("select[name='SpeakerId']")
        .cascadeDropdown({
            from: "LanguageId",
            url: "https://restcountries.eu/rest/v2/lang/",
            parameters: function () {
                return $("#SurveyContainer").find("select[name='LanguageId']").val();
            },
            value: "alpha3Code"
        });

    $("#SurveyComplete").click(function (e) {
        e.preventDefault();
    });
});